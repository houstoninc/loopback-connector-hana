loopback-connector-hana
====

LoopBack connector for official SAP HANA DB client.

How to install?
====

Install the connector package to your project:
```
npm install --save loopback-connector-hana
```

**Note that this connector depends on official HANA DB client (`@sap/hana-client`) that is not available in public
NPM repository. You should download HANA client sources, configure, build and host the NPM package in your own private
NPM repository before installing this connector.**

Set-up a new data source that uses the connector. In `datasources.json`, put the following:
```json
{
  "myDataSourceName": {
    "name": "myDataSourceName",
    "connector": "loopback-connector-hana",
    "endpoint": "YOUR HANA ENDPOINT, FOR EXAMPLE hec.hana.com:30441",
    "username": "HANA SERVER USERNAME",
    "password": "HANA SERVER PASSWORD"
  }
}
```

You are now ready to start using official HANA connector in your LoopBack application!

Connector debug string is `loopback:connector:hana` that can be enabled through `DEBUG` environment variable.

Change log
===

* 0.0.1 -- Initial release.
