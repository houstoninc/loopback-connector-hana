'use strict';
/*
 * Copyright (c) 2018 Houston Inc. Consulting Oy
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

const Connectors = require('loopback-connector');
const inherits = require('util').inherits;
const hana = require('@sap/hana-client');
const hanaStream = require('@sap/hana-client/extension/Stream.js');
const debug = require('debug')('loopback:connector:hana');

exports.initialize = function (dataSource, callback) {
    const settings = dataSource.settings;

    dataSource.connector = new HANAConnector(settings);

    if (callback) {
        dataSource.connector.connect(callback);
    }
};

function HANAConnector(settings) {
    Connectors.SqlConnector.call(this, 'hana', settings);

    this.endpoint = settings.endpoint;
    this.username = settings.username;
    this.password = settings.password;
}

inherits(HANAConnector, Connectors.SqlConnector);

HANAConnector.prototype.connect = function (callback) {
    if (this.endpoint === undefined) {
        return callback(new Error('No endpoint, it must be provided in dataSource.settings'));
    }

    if (this.username === undefined) {
        return callback(new Error('No username, it must be provided in dataSource.settings'));
    }

    if (this.password === undefined) {
        return callback(new Error('No password, it must be provided in dataSource.settings'));
    }

    debug('Connecting to %j', this.endpoint);

    // TODO: do we need a connection pool? If all requests are asynchronous,
    // why would we?
    this.connection = hana.createConnection();

    this.connection.connect({
        serverNode: this.endpoint,
        uid: this.username,
        pwd: this.password
    }, function (error) {
        if (error) {
            debug('Error: %j', error);
        }

        callback(error);
    });
};

HANAConnector.prototype.disconnect = function (callback) {
    this.connection.disconnect(callback);
};

HANAConnector.prototype.toColumnValue = function (propertyDef, value) {
    if (value === null || value === undefined) {
        return 'NULL';
    }

    switch (propertyDef.type.name) {

        // Map native types.
        case 'Number': return value;
        case 'String': return value.replace(/'/g, '\\\'');
        case 'Date': return formatDate(value);
        case 'Boolean': return !!value ? 'true' : 'false';

        // Parse JSON for complex types.
        case 'List':
        case 'Array':
        case 'Object':
        case 'JSON':
            return JSON.stringify(value);
    }

    throw new Error(`Unknown type ${propertyDef.type.name} for value ${value}`);
};

HANAConnector.prototype.fromColumnValue = function (propertyDef, value) {
    if (value === null || value === undefined) {
        return value;
    }

    switch (propertyDef.type.name) {

        // Map native types.
        case 'Number': return value === null || value === undefined ? value : parseFloat(value);
        case 'String': return value.toString ? value.toString() : value;
        case 'Date': return value === null || value === undefined ? value : new Date(value);
        case 'Boolean': return !!value;

        // Parse JSON for complex types.
        case 'List':
        case 'Array':
        case 'Object':
        case 'JSON':
            return JSON.parse(value);
    }

    throw new Error(`Unknown type ${propertyDef.type.name} for value ${value}`);
};

function formatDate(value) {
    return new Connectors.ParameterizedSQL({
        sql: `to_seconddate(?, 'yyyy-mm-dd hh24:mi:ss.ff3')`,
        params: [
            new Date(value).toISOString().replace('T', ' ').replace('Z', '')
        ]
    });
}

HANAConnector.prototype.escapeName = function (name) {
    return `"${name.replace(/"/g, '\\"')}"`;
};

HANAConnector.prototype.getPlaceholderForValue = function (key) {
    return '?';
};

HANAConnector.prototype.applyPagination = function (model, stmt, filter) {
    const limit = this.buildLimit(model, filter.limit, filter.offset || filter.skip);
    return stmt.merge(limit);
};

HANAConnector.prototype.getInsertedId = function (model, info) {
    throw new Error("not implemented");
};

HANAConnector.prototype.executeSQL = function (sql, params, options, callback) {
    debug('SQL: %s, params: %j', sql, params);

    // Use prepare to make safe SQL queries.
    const stmt = this.connection.prepare(sql);

    // Execute the query with parameters and obtain the result set handler.
    const rs = stmt.execQuery(params);

    // Initialize object stream to read the results.
    const objectStream = hanaStream.createObjectStream(rs);

    // Record query start time.
    const t0 = new Date();

    const rows = [];

    objectStream.on('error', function (error) {
        callback(error);
    });

    // We need to read all rows to array because LoopBack does not support
    // streaming results.
    objectStream.on('data', function (data) {
        rows.push(data);
    });

    objectStream.on('end', function () {
        // Measure how long it took to execute the query in seconds.
        const seconds = (new Date().getTime() - t0) / 1000.0;

        debug('Rows: %j, took: %js (%s rows/s).', rows.length, seconds, (rows.length / seconds).toFixed(2));

        // Return results to caller.
        callback(null, rows);
    });
};

// Override tableEscaped to add schema to table names and escape it properly.
HANAConnector.prototype.tableEscaped = function (model) {
    const schemaName = this.schemaEscaped(model);
    let tableName = this.escapeName(this.table(model));

    // Add schema in front of table name.
    if (schemaName !== null) {
        tableName = `${schemaName}.${tableName}`;
    }

    return tableName;
};

HANAConnector.prototype.schemaEscaped = function (model) {
    const schemaName = this.schema(model);
    
    if (schemaName === null || schemaName === undefined)
        return schemaName;

    return this.escapeName(schemaName);
};

HANAConnector.prototype.schema = function (model) {
    const dbMeta = this.getConnectorSpecificSettings(model);

    if (dbMeta) {
        const schemaName = dbMeta.schema || dbMeta.schemaName;

        if (schemaName) {

            // Explicit schema name, return as-is
            return schemaName;
        }
    }

    // No schema definition.
    return null;
};

HANAConnector.prototype.buildLimit = function (model, limit, offset) {
    if (isNaN(limit)) {
        limit = 0;
    }

    if (isNaN(offset)) {
        offset = 0;
    }

    return new Connectors.ParameterizedSQL({
        sql: `LIMIT ? OFFSET ?`,
        params: [limit, offset]
    });
};
